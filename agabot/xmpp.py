#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright (c) 2018 agabot project
# This code is distributed under the GPLv3 License


import logging

import slixmpp
import ssl
from . import api


class SendMsgBot(slixmpp.ClientXMPP):
    """
    A basic SleekXMPP bot that will log in, send a message,
    and then log out.
    """

    def __init__(self, jid, password, room, nick, chatbot_instance, api_instance=None, conf=None):
        slixmpp.ClientXMPP.__init__(self, jid, password)

        # Setup logging.
        logging.basicConfig(level=logging.INFO,
                            format='%(asctime)s - %(levelname)s - %(module)s - %(message)s')

        self.chatbot_instance = chatbot_instance
        self.api_instance = api_instance
        self.earthusers = None
        self.bots = None
        self.botstates = None
        self.clues = conf['agabot']['clues']

        self.room = room
        self.nick = nick

        # The session_start event will be triggered when
        # the bot establishes its connection with the server
        # and the XML streams are ready for use. We want to
        # listen for this event so that we we can initialize
        # our roster.
        self.add_event_handler("session_start", self.start)

        # The groupchat_message event is triggered whenever a message
        # stanza is received from any chat room. If you also also
        # register a handler for the 'message' event, MUC messages
        # will be processed by both handlers.
        self.add_event_handler("groupchat_message", self.muc_message)

        # The message event is triggered whenever a message
        # stanza is received. Be aware that that includes
        # MUC messages and error messages.
        self.add_event_handler("message", self.recv_message)
        # Triggered whenever a presence stanza with a type of subscribe, subscribed, unsubscribe,
        # or unsubscribed is received.
        #
        # Note that if the values xmpp.auto_authorize and xmpp.auto_subscribe are set to True or False, and not None,
        # then SleekXMPP will either accept or reject all subscription requests before your event handlers are called.
        # Set these values to None if you wish to make more complex subscription decisions.
        self.add_event_handler("changed_subscription", self.changed_subscription)

    def set_api_obj(self, earthusers, bots, botstates):
        self.earthusers = earthusers
        self.bots = bots
        self.botstates = botstates

    def start(self, event):
        """
        Process the session_start event.

        Typical actions for the session_start event are
        requesting the roster and broadcasting an initial
        presence stanza.

        Arguments:
            event -- An empty dictionary. The session_start
                     event does not provide any additional
                     data.
        """
        self.send_presence()
        self.get_roster()

        self.plugin['xep_0045'].join_muc(self.room,
                                        self.nick,
                                        # If a room password is needed, use:
                                        # password=the_room_password,
                                        )

    def get_cur_botstate(self, receiver, ):
        find_field = {'jid': receiver}
        id_earthuser = api.item_from_model(model=self.earthusers, find_field=find_field, retrieved_key='id')
        find_field = {'jid': self.jid}
        id_bot = api.item_from_model(model=self.bots, find_field=find_field, retrieved_key='id')
        logging.info("Bot ID: {} and EarthUser ID: {}".format(id_bot, id_earthuser))
        if id_bot and id_earthuser:
            botstate = api.get_botstate(model=self.botstates, bot_id=id_bot,
                                        earthuser_id=id_earthuser)
            return botstate, id_bot, id_earthuser
        else:
            return None, id_bot, id_earthuser

    def changed_subscription(self, presence):
        """
        When a user connects to a bot, it is added to its progression in the profile.
        The botstate json object is retrieved from the user jid and bot jid.
        All the clues are "" by default
        The botstate is get with the api.get_botstate function and it is already a json object.
        The contacted_bot value is changed to true and the api call is performed.
        :param presence: a sleexkmpp presence object
        :return: None
        """
        receiver = str(presence['from'])
        # Refresh the data
        self.earthusers = self.api_instance.get_models('earthuser')
        self.bots = self.api_instance.get_models('bot')
        self.botstates = self.api_instance.get_models('botstate')
        # We have a new contact
        # We must check if he is registred on the website.
        # If he is: OK we add him and perform the api call
        # if not, we send him a message and remove him from our roster
        if 'subscribe' == str(presence.values['type']):
            if receiver in [user['jid'] for user in self.earthusers]:
                print("we have a new valid subscription from {}".format(receiver))
                botstate, id_bot, id_earthuser = self.get_cur_botstate(receiver)
                if botstate:
                    botstate['contacted_bot'] = True
                    self.api_instance.update_object("botstate", botstate, obj_id=botstate['id'])
                    msg = "Bonjour {}".format(receiver)
                    self.send_message(mto=receiver, mbody=msg, mtype='normal')
                    # TODO: add user if to contacts of the bot
                else:
                    logging.ERROR("id bot {} or id user {} is undefined.".format(id_bot, id_earthuser))

            else:
                msg = "{}, vous ne vous êtes pas enregistré sur https://earth-explorer.agayon.be." \
                      " Vous devez créer un acompte avant que nous puissions jouer ensemble.".format(
                    receiver)
                self.send_message(mto=receiver, mbody=msg, mtype='normal')
                self.del_roster_item(receiver)
                self.update_roster(receiver, subscription='remove')

    def muc_message(self, msg):
        """
        Process incoming message stanzas from any chat room. Be aware
        that if you also have any handlers for the 'message' event,
        message stanzas may be processed by both handlers, so check
        the 'type' attribute when using a 'message' event handler.

        Whenever the bot's nickname is mentioned, respond to
        the message.

        IMPORTANT: Always check that a message is not from yourself,
                   otherwise you will create an infinite loop responding
                   to your own messages.

        This handler will reply to messages that mention
        the bot's nickname.

        Arguments:
            msg -- The received message stanza. See the documentation
                   for stanza objects and the Message stanza to see
                   how it may be used.
        """
        if msg['mucnick'] != self.nick and self.nick in msg['body']:
            self.send_message(mto=self.room,
                              mbody="Je t'entends , %s." % msg['mucnick'],
                              mtype='groupchat')
            # TODO: pass it to chatterbot or say something usefull

    def check_user_permissions(self, receiver):
        """
        Check if a user can talk to a bot or not.
        :param receiver: receiver jid
        :return: clearance to talk or not and message
        """
        botstate, id_bot, id_earthuser = self.get_cur_botstate(receiver)
        if not botstate:
            # User has no botstate: he has no profile
            # We must
            # 1: check if the user has no profile (refresh data)
            # 2: if the new data displays a profile:
            # a: check botstate == True
            # b: if botstate == False, return False
            # 3 : if not, return False

            # Refresh data
            self.earthusers = self.api_instance.get_models('earthuser')
            self.botstates = self.api_instance.get_models('botstate')
            botstate, id_bot, id_earthuser = self.get_cur_botstate(receiver)
            if not botstate:
                # User has still no profile
                msg = "{}, vous ne vous êtes pas enregistré sur https://earth-explorer.agayon.be/profile." \
                      " Vous devez créer un compte avant que nous puissions jouer ensemble. " \
                      "Si votre compte a été créé " \
                      "dans l'intervalle, déconnectez-vous et reconnectez-vous.".format(receiver)
                return False, msg, botstate, id_bot, id_earthuser
        # User has a profile but he is not authorizedto talk to the bot
        if not botstate['contacted_bot']:
            # refresh data
            self.botstates = self.api_instance.get_models('botstate')
            botstate, id_bot, id_earthuser = self.get_cur_botstate(receiver)
            if not botstate['contacted_bot']:
                msg = "Vous ne semblez pas être en mesure de parler à ce bot. " \
                      "Connectez-vous sur https://earth-explorer.agayon.be/profile pour parler à un " \
                      "bot de votre niveau. N'oubliez pas d'ajouter le bot à vos amis pour déclencher " \
                      "la conversation."
                return False, msg, botstate, id_bot, id_earthuser
            # After refreshing data, user can actually talk to the bot
            else:
                return True, False, botstate, id_bot, id_earthuser
        # user is free to talk to the bot
        else:
            return True, False, botstate, id_bot, id_earthuser

    def recv_message(self, msg):
        """
        When the bot receive a message, it is processed and a responsed is generated by the chatbot.
        If the response contains a new clue, an api call is performed to update the list of clues in the django app.
        :param msg: a xmpp message
        :return: None
        """
        if msg['type'] in ('chat', 'normal',):
            receiver = str(msg['from'].bare)
            ret, response, botstate, id_bot, id_earthuser = self.check_user_permissions(receiver)
            # if response == False, we can build a bot response. Else, it answer with the error message
            if ret and not response:
                response = self.chatbot_instance.chatbot_response(msg['body'])
                response = str(response)
                for idx, value in self.clues.items():
                    if value['trigger'].lower() in response.lower():
                        if botstate:
                            # botstate of clue idx is now True: we have discovered the idx th clue.
                            try:
                                botstate['clue' + str(idx)] = True
                                self.api_instance.update_object("botstate", botstate, obj_id=botstate['id'],)
                                logging.info("Update clue {}: {} for user {}".format(idx, value['clue'], receiver))
                            except TypeError:
                                pass

            msg.reply(response).send()
            # msg.reply("Thanks for sending\n%(body)s" % msg).send()
            # msg.reply("Thanks for sending\n%(body)s" % msg).send()


def send_muc(self, msg):
    m = 'send_muc: ' + msg + ' ' + self.room
    self.send_message(mto=self.room,
                      mbody=msg,
                      mtype='groupchat')


def set_status(self, status, status_msg):
    self.send_presence(pstatus=status_msg, pshow=status)


def set_triggers_clues(clues):
    """
    Create a dictionnary of triggers
    :param clues: list of dictionnary of clues
    :return: triggers list
    """
    triggers = []
    for idx, clue in clues.items():
        triggers.append(clue['trigger'])
    return triggers


def init_xmpp(conf=None, chatbot_instance=None, api_instance=None, earthusers=None, botstates=None, bots=None):
    config_xmpp = conf['xmpp']
    # FIXME : read clues and triggers from API. Update conf with the values like before.
    xmpp_instance = SendMsgBot(config_xmpp['jid'], config_xmpp['password'], config_xmpp['room'], config_xmpp['nick'],
                               chatbot_instance, api_instance=api_instance, conf=conf)
    logging.getLevelName('XMPP')

    # split every xmpp_register_plugin and load them in a loop.
    # this xep provide ping sending to keep the connection alive
    xmpp_instance.register_plugin('xep_0199', {'keepalive': True,
                                               'frequency': 15})
    xmpp_instance.ssl_version = ssl.PROTOCOL_TLSv1_2
    for plugin in config_xmpp['plugins']:
        xmpp_instance.register_plugin(plugin)

    xmpp_instance.set_api_obj(earthusers=earthusers, botstates=botstates, bots=bots)
    xmpp_instance.connect()
        # block=True will block the current thread. By default, block=False

    xmpp_instance.process()
    # else:
    #     print("Unable to connect.")
    #     xmpp_instance = None

    return xmpp_instance
