#!/usr/bin/env python
# Copyright (c) 2018 agabot project
# This code is distributed under the GPLv3 License

# -*- coding: utf-8 -*-

import configparser
import os
import logging

logger = logging.getLogger(__name__)

def read_config(filename='example.ini'):
    if not os.path.exists(filename):
        logger.error("The config file does not exists !")
        return None
    config = configparser.ConfigParser()
    config.read(filename)
    section = 'XMPP'
    room = config.get(section, 'room')
    jid = config.get(section, 'jid')
    password = config.get(section, 'password')
    nick = config.get(section, 'nick')
    plugins = config.get(section, 'plugins')
    plugins = plugins.split(" ")
    section = 'AGABOT'
    name = config.get(section, 'name')
    description = config.get(section, 'description')
    number = config.getint(section, 'number')
    corpus = config.get(section, 'corpus')
    corpus = corpus.split(" ")
    if not os.path.exists(corpus[0]):
        logger.error("The corpus file does not exists !")
        return None
    clue0 = config.get(section, 'clue0')
    clue1 = config.get(section, 'clue1')
    clue2 = config.get(section, 'clue2')
    clue3 = config.get(section, 'clue3')
    clue4 = config.get(section, 'clue4')
    clues = {}
    for idx, c in enumerate([clue0, clue1, clue2, clue3, clue4]):
        cl = c.split("|")
        trigger = cl[0]
        clue = cl[1]
        clues[idx] = {'trigger': trigger, 'clue': clue}
    section = 'REST_API'
    api_user = config.get(section, 'user')
    api_password = config.get(section, 'password')
    url = config.get(section, 'url')
    token = config.get(section, 'token')
    configuration = {'xmpp': {'room': room, 'jid': jid, 'password': password,
                     'nick': nick, 'plugins': plugins},
            'agabot': {'name': name, 'corpus': corpus, 'clues': clues, 'description': description, 'number': number},
            'api': {'user': api_user, 'password': api_password, 'url': url, 'token': token}
            }

    return configuration


if __name__ == '__main__':
    conf = read_config('../config.example.ini')
