#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright (c) 2018 agabot project
# This code is distributed under the GPLv3 License


import requests
from urllib.parse import urljoin
import logging
from .credentials import token, password, user

logging.getLevelName('XMPP')


class RestAPI:
    def __init__(self, user=None, password=None, url=None, token=None, dev=False):
        if user:
            self.user = user
        if password:
            self.password = password
        if url:
            self.url = url
        if token:
            self.token = token
        if dev:
            self.dev = dev
        if user and password:
            self.auth = (self.user, self.password)
        else:
            self.auth = None
        if token:
            self.headers = {'Authorization': 'Token {}'.format(token)}
        else:
            self.headers = None

    def get_models(self, model):
        url = urljoin(self.url, model)
        r = requests.get(url, auth=self.auth, headers=self.headers)
        if r.status_code == 200:
            return r.json()
        else:
            logging.debug(r.text)
            logging.error(f"Empty response: {model}")
            return None

    def get_details(self, model='', obj_id=0, ):
        url = urljoin(self.url, model) + '/'
        url = urljoin(url, str(obj_id))

        r = requests.get(url, auth=self.auth, headers=self.headers)
        try:
            return r.json()
        except ValueError:  # includes simplejson.decoder.JSONDecodeError
            print(r.text)
            logging.error("EMPTY response")
            return {}

    def update_object(self, model, json_data, obj_id=None):
        url = urljoin(self.url, model) + '/'
        try:
            if not obj_id:
                ret = requests.post(url, auth=self.auth, json=json_data, headers=self.headers)
            else:
                url = urljoin(url, str(obj_id)) + '/'
                ret = requests.put(url, auth=self.auth, json=json_data, headers=self.headers)
            if not ret.status_code == 200:
                print(ret.text)
        except requests.exceptions.RequestException:
            logging.error("Request failed {}".format(url))


def get_botstate(model=None, bot_id=None, earthuser_id=None):
    """
    Get a botstate from the list of botstates
    :param model: the dictionnary of botstates
    :param bot_id: the id of the bot
    :param earthuser_id: the id of the user
    :return: the botstate json dictionnary
    """
    find_field_bot = {'bot': bot_id}
    res_filtered = item_from_model(model=model, find_field=find_field_bot, )
    find_field_user = {'earth_user': earthuser_id}
    botstate = item_from_model(model=res_filtered, find_field=find_field_user, )
    try:
        return botstate[0]
    except IndexError:
        print("No botstate found")
        return None


def item_from_model(model=None, find_field=None, retrieved_key=None):
    if not find_field:
        find_field = {}
    # it only works with single key and value in find_field
    key = list(find_field.keys())[0]
    value = find_field[key]
    try:
        result = [item for item in model if item[key] == value]
    except TypeError:
        logging.error("Error: no item in model. Something went wrong !")
        logging.error("Model {}, Find field: {}, Key: {}".format(str(model), str(find_field), str(retrieved_key)))
        return None
    if retrieved_key:
        try:
            result_value = result[0][retrieved_key]
            return result_value
        except (IndexError, KeyError):
            return None
    else:
        return result


def test():
    url = 'https://earth-explorer.agayon.be/api/'
    dev = True
    # api_instance = RestAPI(user=user, password=password, url=url, token=token)
    api_instance = RestAPI(url=url, token=token, dev=dev, user=user, password=password)
    # res = rest.get_models('bots')
    # logging.debug(res)
    # res = api_instance.get_details('earthusers', 3)
    botstates = api_instance.get_models('botstate')
    id_earthuser = 2
    id_bot = 1
    botstate = get_botstate(model=botstates, bot_id=id_bot,
                            earthuser_id=id_earthuser)
    data = botstate
    data['contacted_bot'] = True
    clue = 'hello you ! lol'
    for c in ['clue0', 'clue1', 'clue2', 'clue3', 'clue4']:
        data[c] = clue
    # data_json = json.dumps(data)
    # print(data_json)
    # data_json = json.loads(data_json)
    data_json = data
    api_instance.update_object("botstates", str(data['id']), data_json)


def test2():
    myurl = "http://127.0.0.1:8000/api/bots"
    # A get request (json example):
    response = requests.get(myurl, headers={'Authorization': 'Token {}'.format(token)})
    data = response.json()
    print(data)


def test3():
    myurl = "https://earth-explorer.agayon.be/api/bots"
    # A get request (json example):
    response = requests.get(myurl, headers={'Authorization': 'Token {}'.format(token)}, auth=(user, password))
    data = response.json()
    print(data)


if __name__ == '__main__':
    test3()
