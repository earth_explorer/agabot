#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Copyright (c) 2018 agabot project
# This code is distributed under the GPLv3 License
from chatterbot import ChatBot
from chatterbot.trainers import ListTrainer
from chatterbot.response_selection import get_random_response, get_first_response
from chatterbot.trainers import ChatterBotCorpusTrainer
import random
from agabot import api

'''
This generic chatterbot class is trained with corpus specified in config file and interract throught xmpp.
'''


class Agabot:
    def __init__(self, corpus_list, name, ):
        self.corpus_list = corpus_list
        self.name = name
        self.chatbot = None
        self.stop = None
        self.monosyllabic = 0

    def prepare(self, conf):
        self.chatbot = ChatBot(
            self.name,
            database_uri='sqlite:///{}.sqlite3'.format(self.name),
            storage_adapter='chatterbot.storage.SQLStorageAdapter',
            logic_adapters=[
                {
                    "import_path": "chatterbot.logic.BestMatch",
                    # "statement_comparison_function": "chatterbot.comparisons.levenshtein_distance",
                    "response_selection_method": get_random_response
                }
            ],
            trainer='chatterbot.trainers.ChatterBotCorpusTrainer',
            tie_breaking_method="random_response", )

        # Start by training our bot with the ChatterBot corpus data
        # self.trainer = ListTrainer(self.chatbot)
        self.trainer = ChatterBotCorpusTrainer(self.chatbot)
        for corpus in self.corpus_list:
            print(corpus)
            self.trainer.train(corpus)
        self.chatbot.read_only = True

    def chatbot_response(self, user_msg):
        response = self.chatbot.get_response(user_msg)
        if user_msg.count(' ') < 1:
            self.monosyllabic += 1
        if self.monosyllabic >= 5:
            response = "Je ne suis pas très malin mais je ne suis pas idiot non plus. " \
                       "Tu peux faire des phrases un peu plus longues. Je te répondrai."
            self.monosyllabic = 0
        return response

    def chatbot_instance(self):
        return self.chatbot
