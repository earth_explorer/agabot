#!/usr/bin/env python
# Copyright (c) 2018 agabot project
# This code is distributed under the GPLv3 License

# -*- coding: utf-8 -*-

from agabot import xmpp
from agabot import config_parser
from agabot import bot
from agabot import api
import logging
import argparse
from agabot import config_bot

import json

FORMAT = '%(asctime)-15s %(message)s'
logging.basicConfig(format=FORMAT)


def launcher():
    parser = argparse.ArgumentParser(description='Chatbot')
    parser.add_argument("-f", "--file",
                        help='Config file containing XMPP parameters',
                        required=False, default='config.ini')
    parser.add_argument("-d", "--debug", help="set logging to DEBUG",
                        action="store_const", dest="loglevel",
                        const=logging.DEBUG, default=logging.ERROR)
    parser.add_argument("-i", "--initialize", help="Update or create bot based on config file data",
                        action="store_const", dest="init",
                        const=True, default=False)

    args = parser.parse_args()
    logging.basicConfig(level=args.loglevel, format='%(levelname)-8s %(message)s')

    debug = False
    if args.loglevel == logging.DEBUG:
        debug = True

    local_conf = config_parser.read_config(args.file)
    # Read the bots configuration and save it.
    if not local_conf:
        return None
    api_instance = api.RestAPI(user=local_conf['api']['user'], password=local_conf['api']['password'],
                               url=local_conf['api']['url'],
                               token=local_conf['api']['token'])
    # Get config of bot: clues and triggers. Update the conf dictionnary
    ret = api_instance.get_models('bot')
    if ret is not None:
        # Api can be joined
        earthusers = api_instance.get_models('earthuser')
        bots = api_instance.get_models('bot')
        botstates = api_instance.get_models('botstate')
        if args.init:
            # 1. We have bots and the current bot exists upstream. We update the fields with the local configuration.
            bot_id = api.item_from_model(model=bots, find_field={'jid': local_conf['xmpp']['jid']}, retrieved_key='id')
            update_data = config_bot.prepare_local_bot_config(local_conf, bot_id)
            if bots and bot_id:
                api_instance.update_object("bot", json_data=update_data, obj_id=bot_id)
            else:
                update_data.pop('id', None)
                api_instance.update_object("bot", json_data=update_data)
        else:
            upstream_conf = config_bot.get_bots_items(local_conf, api_instance)
            if not upstream_conf:
                logging.critical(
                    "This bot is not registrered on earth-explorer.agayon.be. Please relaunch it with the '-i' option")
                return None
            upstream_conf['earthuser'] = earthusers
            upstream_conf['bot'] = bots
            upstream_conf['botstate'] = botstates
            chatbot_instance = bot.Agabot(upstream_conf['agabot']['corpus'], upstream_conf['agabot']['name'], )
            chatbot_instance.prepare(upstream_conf, )
            xmpp_instance = xmpp.init_xmpp(conf=upstream_conf, chatbot_instance=chatbot_instance,
                                           api_instance=api_instance,
                                           earthusers=earthusers, botstates=botstates, bots=bots)

    else:
        logging.critical("No API available")
        return None


if __name__ == '__main__':
    launcher()
