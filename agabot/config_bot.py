#!/usr/bin/env python
# Copyright (c) 2018 agabot project
# This code is distributed under the GPLv3 License

# -*- coding: utf-8 -*-

import requests
import copy
from . import api
from requests.exceptions import ConnectionError

import logging

logger = logging.getLogger(__name__)

def get_bots_items(local_conf=None, api_instance=None):
    """
    This function aims to retrieve the bot parameters: triggers and clues.
    The name of the bot is provided. The api is called with the right ID and data are processed
    :param conf: partial config readfrom the ini file
    :param api_instance: the api class instance used to query the rest API.
    :return: the config with triggers and clues for the running bot.
    """

    find_field = {'jid': local_conf['xmpp']['jid']}
    # When the dict are not deeply copied, they point to the same object and local is updated when upstream is modified
    upstream_conf = copy.deepcopy(local_conf)
    clues = {}
    try:
        bots = api_instance.get_models('bot')
        id_bot = api.item_from_model(model=bots, find_field=find_field, retrieved_key='id')
        if id_bot:
            current_bot = api_instance.get_details(model='bot', obj_id=id_bot)
            for n in range(5):
                # Get clues and trigger from 0 to 4
                n_trigger = 'trigger' + str(n)
                n_clue = 'clue' + str(n)
                trigger = current_bot[n_trigger]
                clue = current_bot[n_clue]
                clues[n] = {'trigger': trigger, 'clue': clue}
            upstream_conf['agabot']['clues'] = clues
            return upstream_conf
        else:
            logging.error("Could not connect to the API.")
            return None
    except (ConnectionError, ConnectionRefusedError):
        logging.critical("Unavailable API")
        return None


def prepare_local_bot_config(local_conf, bot_id):
    update_data = {"id": bot_id, "name": local_conf['agabot']['name'],
                   "jid": local_conf['xmpp']['jid'], 'description': local_conf['agabot']['description'],
                   'number': local_conf['agabot']['number']}
    for n in range(5):
        current_clue_key = f"clue{n}"
        current_trigger_key = f"trigger{n}"
        current_clue_value = local_conf['agabot']['clues'][n]['clue']
        current_trigger_value = local_conf['agabot']['clues'][n]['trigger']

        update_data[current_clue_key] = current_clue_value
        update_data[current_trigger_key] = current_trigger_value

    return update_data
