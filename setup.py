#!/usr/bin/env python
# Copyright (c) 2018 agabot project
# This code is distributed under the GPLv3 License
# The code is inspired from https://github.com/rbarrois/aionotify

from setuptools import setup, find_packages
from codecs import open
from os import path

here = path.abspath(path.dirname(__file__))

try:
    import pypandoc

    long_description = pypandoc.convert(path.join(here, 'README.md'), 'rst')
except(IOError, ImportError):
    long_description = open(path.join(here, 'README.md'), encoding='utf-8').read()

# Arguments marked as "Required" below must be included for upload to PyPI.
# Fields marked as "Optional" may be commented out.

setup(
    name='agabot',  # Required
    version='0.2',  # Required
    description='Agabot is a chatterbot used in the game [earth-explorer](https://earth-explorer.agayon.be).',
    long_description=long_description,  # Optional
    url='https://gitlab.com/earth_explorer/agabot',  # Optional
    author='Arnaud Joset',  # Optional
    author_email='info@agayon.be',
    classifiers=[
        # https://pypi.python.org/pypi?%3Aaction=list_classifiers
        'Development Status :: 3 - Alpha',
        'Environment :: Console',
        'Intended Audience :: Developers',
        'Topic :: Internet :: XMPP',
        'Topic :: Communications',
        'License :: OSI Approved :: GNU General Public License v3 (GPLv3)',
        'Programming Language :: Python :: 3 :: Only',
        'Operating System :: POSIX :: Linux',
    ],
    keywords='xmpp chat bot',  # Optional
    packages=find_packages(exclude=[]),
    install_requires=['chatterbot_corpus', 'ChatterBot', 'sleekxmpp'],
    python_requires='>=3.4',
    # If there are data files included in your packages that need to be
    # installed, specify them here.
    # package_data={
    #    'agabot': [],
    # },

    # To provide executable scripts, use entry points in preference to the
    # "scripts" keyword. Entry points provide cross-platform support and allow
    # `pip` to create the appropriate form of executable for the target
    # platform.
    #
    # For example, the following would provide a command called `sample` which
    # executes the function `main` from this package when invoked:
    entry_points={  # Optional
        'console_scripts': [
            'agabot=agabot.agabot_launcher:launcher',
        ],
    },
)
